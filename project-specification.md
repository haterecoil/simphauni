framework php
symfony

. la promotion (os, jobs, xp)
. php (langage, pourquoi en 2022)
. framework ()
. symfony 6 (pourquoi 6?, archi)

. choix d'un projet

. installation


Installation : 
- PHP 8.1
- Composer
- Symfony CLI

Ensuite on peut créer un projet 


6 séances
1. Introduction
2. Formulaires et Entités 
3. Sécurité
4. Migrations
5. Assets
6. Data


Spécifications fonctionnelles 

Le projet de l'année serait un site e-commerce. 

Un site e-commerce fonctionnalités "front-office" : 
- faire des achats (donc avoir un panier)
- créer un compte client (avec un histrique de commandes)

Un site e-commerce fonctionnalités "back-office" : 
- gérer des produits
- gérer des commandes 

Les entités :

- Client
    - ID
    - email

- Adresses
    - ID
    - Client
    - Type : livraison || facturation 
    - infos classiques d'adresse

- Produit
    - Prix
    - Quantité en stock
    - ID
    - SKU (référence interne - stock keeping unit)
    - Nom
    - Image
    - Date de livraison la plus proche possible

- Panier
    - Produits
    - Clients

- Commande
    - Panier
    - Transporteur 
    - Etat de commande


POUR LE 17 OCTOBRE 
- Morgan
    - fournir un dossier "boilerplate" mardi 4 octobre
    - morgan@quanticfy.io
- E3
    - Docker installé sur toutes les machines
        - faisable le 3 octobre
        - beaucoup n'ont pas d'alternance / ont du temps libre --> objectif avancer 
        - créer un canal de communication en commun
    - boilerplate fonctionnel
    - bonus: https://symfony.com/doc/current/page_creation.html
    - bonus: lire (ou adapter) https://openclassrooms.com/fr/courses/5489656-construisez-un-site-web-a-l-aide-du-framework-symfony-5 


# Formulaires et entités 

- architecture de base 
- les vues, les templates
- les controllers
- les formulaires

bonus :
- entités


## Roadmap

1. Constituer un répertoire de projet : 
	a. docker
	b. php
	c. nginx
	d. mysql
2. Créer notre premier Controller
3. Créer notre première page (Template + Route)
4. Ajouter un formulaire simple (Ajout au panier ?)
5. Sauvegarder une entité

----
