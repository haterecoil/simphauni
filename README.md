# Pourquoi ce nom ?

simphauni

J'ai pas choisi

# Qu'est-ce qu'on va faire

un framework en PHP :) 

# Objectif lundi 31 octobre

Une page "contact"
- formulaire avec :
  - email
  - sujet
  - contenu
- 

# Démarrage

Clone 

```bash
git clone https://gitlab.com/haterecoil/simphauni.git;

# installer composer si besoin
wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php -- --quiet

# installer les dépendances composer
composer install; # ou `php composer.phar install`
```

Lancer le serveur : 

```bash
php -S 127.0.0.1:8081
```

Si vous avez un problème de port / de permission : changez le port.

# Architecture

- index.php     // le point d'entrée principal de mon application
- src
  - Controller
    - controller.php
  - router.php
- templates
  - items
    - index.php
  - home.php

## Routage

http://127.0.0.1:3000/?route={nom_de_la_route}

## Composer

Pour CEUX QUI N'ONT PAS COMPOSER :
- suivre les instructions de https://getcomposer.org/download/
- vous devriez avoir dans votre dossier simpahuni un fichier composer.phar

Ensuite, on crée un projet composer 

```bash
# ceux qui avaient déjà composer
composer init;
# ceux qui viennent de l'installer;
php composer.phar init;

# après avoir votre cmposer.json
# il faut le rafraichir
composer dump-autoload;

# installer composer
composer require "twig/twig:3.4";

composer require symfony/form;

composer require symfony/twig-bridge;
```
