<?php

require_once 'vendor/autoload.php';

use Simphauni\Controller\Controller;
use Simphauni\Router;
use Symfony\Component\Form\Forms;

// va contenir les liens route->page
$router = new Router();

// pour chaque page, on crée un controller
$controller = new Controller();
$controller->setRoute('/');   
$controller->setPage('index.twig');

// on ajoute le controller au router
$router->addController($controller);


$controller = new Controller();
$controller->setRoute('/articles');
$controller->setPage('templates/articles/index.php'); // @todo adapter cette route à twig

// on ajoute le controller au router
$router->addController($controller);

// START TWIG

$loader = new \Twig\Loader\FilesystemLoader( __DIR__ . '/templates');
$twig = new \Twig\Environment($loader, []);

$templatePath = $router->getPageFromRoute($_GET['route']);

$template = $twig->load($templatePath);

$formFactory = Forms::createFormFactory();

$form = $formFactory->createBuilder()
    ->add('username', TextType::class)
    ->getForm();

echo $template->render([
    'mavar' => 'aloha', 
    'form' => $form->createView()
]);