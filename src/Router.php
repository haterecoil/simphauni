<?php

namespace Simphauni;

use Simphauni\Controller\Controller as Controller;

/**
 * Router regroupe les controllers et permet de trouver le bon controller
 * pour une route donnée.
 * 
 */
class Router
{

    // on décide d'utilise un tableau associatif pour stocker le lien
    // entre route et controller
    // @var [string]Controller controllers
    private array $controllers = [];

    public function addController(Controller $controller)
    {
        $this->controllers[$controller->getRoute()] = $controller;
    }

    public function getPageFromRoute(string $route)
    {
        // @todo que faire si $route n'existe pas ?
        $pagePath = $this->controllers[$route]->getPage();
        return $pagePath;
    }

}