<?php 

namespace Simphauni\Controller;

/**
 * Controller :
 * - stocke le lien entre une route et une page
 * - traite la requête HTTP
 */
class Controller 
{
    // $route contient un chemin d'accès 
    // exemple: "/", "/index", "/items" ...
    private string $route;

    // $page contient le nom d'une page
    private string $page;

    public function setRoute(string $route)
    {
        $this->route = $route;
    }

    public function getRoute(): string
    {
        return $this->route;
    }

    public function setPage(string $page)
    {
        $this->page = $page;
    }

    public function getPage(): string
    {
        return $this->page; 
    }
}